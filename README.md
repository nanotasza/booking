1. Pull and run Docker container with Postgres
`docker pull postgres`
`docker run --rm   --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432  postgres`

Database is populated automatically on application startup.

2. Run tests:

`mvn test`

Only unit tests :( I had some problem running integration tests with rest assured against temporary docker container, and I gave up...

3. Build app:

 `mvn install -DskipTests`

4. Run app:

`java -jar target/booking-0.0.1-SNAPSHOT.jar `

5. Usage:

UUIDs of entities can be found in data.sql

a) register user

`curl -X POST \
  http://localhost:8080/customers \
  -H 'Content-Type: application/json' \
  -d '{"firstName" : "Derek",
"lastName" : "Daikon"}'`

b) search rooms:

 `curl -X GET 'http://localhost:8080/rooms?dateFrom=2019-10-01&dateTo=2019-10-02&city=Warsaw&priceFrom=50.0&priceTo=160.0'`
where dates are in ISO date format

c) request booking:

`curl -X POST \
  http://localhost:8080/bookings \
  -H 'Content-Type: application/json' \
  -d '{"dateFrom" : "2019-11-10",
"dateTo" : "2019-11-12",
"roomId" : "46a223a8-bb28-44d7-9f65-db1ca19a4710",
"customerId" : "78a223a8-bb28-44d7-9f65-db1ca19a4710"
}'`

Response HttpStatus CONFLICT if dates are overlapping.

Response HttpStatus NOT_FOUND if room or customer don't exist.

d) get bookings for a customer:

`curl -X GET http://localhost:8080/customers/10002/bookings`

e) cancel booking:
`curl -X PUT \
  http://localhost:8080/bookings/1 \
  -H 'Content-Type: application/json' \
  -d '{
"bookingStatus" : "CANCELLED"
}'`

Note: This endpoint ignores other fields in the body than "bookingStatus" (since the only requirement was to cancel the booking). It could be extended further to change other data, which would require data consistency checks.

Response HttpStatus NOT_FOUND if  customer doesn't exist.

