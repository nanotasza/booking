package com.nano.booking.service;

import com.nano.booking.entity.BookingEntity;
import com.nano.booking.exception.RoomAlreadyBookedForThisPeriodException;
import com.nano.booking.exception.ResourceNotFoundException;
import com.nano.booking.model.Booking;
import com.nano.booking.model.BookingDto;
import com.nano.booking.model.BookingStatus;
import com.nano.booking.repository.BookingRepository;
import com.nano.booking.repository.CustomerRepository;
import com.nano.booking.repository.RoomRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class BookingService {
    private final BookingRepository bookingRepository;
    private final CustomerRepository customerRepository;
    private final RoomRepository roomRepository;

    @Autowired
    public BookingService(BookingRepository bookingRepository, CustomerRepository customerRepository, RoomRepository roomRepository) {
        this.bookingRepository = bookingRepository;
        this.customerRepository = customerRepository;
        this.roomRepository = roomRepository;
    }

    public void createBooking(BookingDto booking) {
        if (!bookingRepository.getOverlappingBookings(booking.getRoomId(), booking.getDateFrom(), booking.getDateTo()).isEmpty()) {
            throw new RoomAlreadyBookedForThisPeriodException();
        }
        BookingEntity bookingEntity = new BookingEntity();
        bookingEntity.setDateFrom(booking.getDateFrom());
        bookingEntity.setDateTo(booking.getDateTo());
        bookingEntity.setBookingStatus(BookingStatus.CONFIRMED);
        bookingEntity.setCustomer(customerRepository.findById(booking.getCustomerId()).orElseThrow(() -> new ResourceNotFoundException("customer")));
        bookingEntity.setRoom(roomRepository.findById(booking.getRoomId()).orElseThrow(() -> new ResourceNotFoundException("room")));
        bookingRepository.save(bookingEntity);
    }

    public Iterable<Booking> getAllByCustomer(UUID customerId){
        List<BookingEntity> bookings = bookingRepository.getAllByCustomer_Id(customerId);
        return bookings
                .stream()
                .map(booking -> new ModelMapper().map(booking, Booking.class))
                .collect(Collectors.toList());
    }

    public void updateBooking(UUID id, BookingDto booking) {
        BookingEntity existingBooking = bookingRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("booking"));
        existingBooking.setBookingStatus(booking.getBookingStatus());
        // TODO: update other fields, and remember to check if room is available for date
        bookingRepository.save(existingBooking);
    }
}
