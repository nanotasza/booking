package com.nano.booking.service;

import com.nano.booking.entity.RoomEntity;
import com.nano.booking.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Component
public class RoomService {
    private final RoomRepository roomRepository;

    @Autowired
    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public Set<RoomEntity> getAvailableRooms(String city, LocalDate dateFrom, LocalDate dateTo, BigDecimal priceFrom, BigDecimal priceTo){
        return roomRepository.getMatchingRooms(city, dateFrom, dateTo, priceFrom, priceTo);
    }

    public Iterable<RoomEntity> findAll(){
        return roomRepository.findAll();
    }
}
