package com.nano.booking.controller;

import com.nano.booking.model.BookingDto;
import com.nano.booking.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.UUID;

@RestController
@RequestMapping("/bookings")
public class BookingController {

    private final BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void requestBooking(@RequestBody BookingDto booking){
        bookingService.createBooking(booking);
    }

    @PutMapping("/{bookingId}")
    @ResponseStatus(HttpStatus.OK)
    void updateBooking(@PathVariable UUID bookingId, @RequestBody BookingDto booking){
        bookingService.updateBooking(bookingId, booking);
    }
}
