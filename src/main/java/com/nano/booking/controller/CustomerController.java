package com.nano.booking.controller;

import com.nano.booking.model.Booking;
import com.nano.booking.model.Customer;
import com.nano.booking.service.BookingService;
import com.nano.booking.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.awt.print.Book;
import java.util.UUID;

@RestController
@RequestMapping("/customers")
public class CustomerController {
    private final CustomerService customerService;
    private final BookingService bookingService;

    @Autowired
    public CustomerController(CustomerService customerService, BookingService bookingService) {
        this.customerService = customerService;
        this.bookingService = bookingService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void registerCustomer(@RequestBody Customer customer){
        customerService.registerCustomer(customer);
    }

    @GetMapping("/{customerId}/bookings")
    public Iterable<Booking> getBookings(@PathVariable UUID customerId) {
        return bookingService.getAllByCustomer(customerId);
    }
}
