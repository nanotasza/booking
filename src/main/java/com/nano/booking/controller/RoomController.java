package com.nano.booking.controller;

import com.nano.booking.service.RoomService;
import com.nano.booking.entity.RoomEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@RestController
@RequestMapping("/rooms")
public class RoomController {
    private final RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @GetMapping
    public Set<RoomEntity> getMatchingRooms(
            @RequestParam(name="city") String city,
            @RequestParam(name = "dateFrom")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                    LocalDate dateFrom,
            @RequestParam(name = "dateTo")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                    LocalDate dateTo,
            @RequestParam(name="priceFrom")BigDecimal priceFrom,
            @RequestParam(name="priceTo")BigDecimal priceTo) {
        return roomService.getAvailableRooms(city, dateFrom, dateTo, priceFrom, priceTo);
    }
}
