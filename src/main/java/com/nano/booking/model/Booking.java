package com.nano.booking.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nano.booking.entity.CustomerEntity;
import com.nano.booking.entity.RoomEntity;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class Booking {
    private UUID id;

    @JsonIgnoreProperties("bookings")
    private CustomerEntity customer;

    @JsonIgnoreProperties("bookings")
    private RoomEntity room;

    private LocalDate dateFrom;

    private LocalDate dateTo;

    private BookingStatus bookingStatus;
}
