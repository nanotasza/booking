package com.nano.booking.model;

import com.nano.booking.entity.CustomerEntity;
import com.nano.booking.entity.RoomEntity;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
public class BookingDto {
    private UUID customerId;
    private UUID roomId;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private BookingStatus bookingStatus = BookingStatus.CONFIRMED;
}
