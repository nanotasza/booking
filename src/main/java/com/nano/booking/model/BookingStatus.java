package com.nano.booking.model;

public enum BookingStatus {
    CONFIRMED,
    CANCELLED
}
