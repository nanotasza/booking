package com.nano.booking.model;

import com.nano.booking.entity.BookingEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Data
public class Customer {
    private UUID id;
    private String firstName;
    private String lastName;
    private Set<BookingEntity> bookings;
}
