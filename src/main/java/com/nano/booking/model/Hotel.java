package com.nano.booking.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
public class Hotel {
    private UUID id;
    private String city;
}
