package com.nano.booking.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nano.booking.entity.BookingEntity;
import com.nano.booking.entity.HotelEntity;
import com.nano.booking.serializer.MoneyDeserializer;
import com.nano.booking.serializer.MoneySerializer;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

@Data
public class Room {
    private UUID id;
    private BigDecimal pricePerNight;
    private Set<BookingEntity> bookings;
    private HotelEntity hotel;
}
