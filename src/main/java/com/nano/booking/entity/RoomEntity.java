package com.nano.booking.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nano.booking.serializer.MoneyDeserializer;
import com.nano.booking.serializer.MoneySerializer;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@Table(name = "room")
public class RoomEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @JsonSerialize(using = MoneySerializer.class)
    @JsonDeserialize(using= MoneyDeserializer.class)
    private BigDecimal pricePerNight;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL)
    private Set<BookingEntity> bookings;

    @ManyToOne
    @JoinColumn
    @JsonIgnoreProperties("rooms")
    private HotelEntity hotel;
}
