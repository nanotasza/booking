package com.nano.booking.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

public class MoneyDeserializer extends StdDeserializer<BigDecimal> {
    public MoneyDeserializer(){
        this(BigDecimal.class);
    }

    public MoneyDeserializer(Class<BigDecimal> val) {
        super(val);
    }

    @Override
    public BigDecimal deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return new BigDecimal(jsonParser.getValueAsString());
    }
}
