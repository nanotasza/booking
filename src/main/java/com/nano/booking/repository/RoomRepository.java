package com.nano.booking.repository;

import com.nano.booking.entity.RoomEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Repository
public interface RoomRepository extends CrudRepository<RoomEntity, UUID> {

    @Query("select distinct r from RoomEntity r " +
            "left join " +
            "r.bookings b on r.id = b.room.id " +
            "where " +
            "((:dateFrom > b.dateTo or b.dateFrom > :dateTo) " +
            "or " +
            "(:dateFrom <= b.dateTo and b.dateFrom <= :dateTo and b.bookingStatus=1) " +
            "or " +
            "(b.dateFrom is null and b.dateTo is null)) " +
            "and r.hotel.city = :city " +
            "and r.pricePerNight > :priceFrom and r.pricePerNight < :priceTo")
    Set<RoomEntity> getMatchingRooms(String city, LocalDate dateFrom, LocalDate dateTo, BigDecimal priceFrom, BigDecimal priceTo);
}
