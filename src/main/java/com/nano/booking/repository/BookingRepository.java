package com.nano.booking.repository;

import com.nano.booking.entity.BookingEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface BookingRepository extends CrudRepository<BookingEntity, UUID> {

    List<BookingEntity> getAllByCustomer_Id(UUID id);

    @Query("select b from BookingEntity b where b.bookingStatus=0 " +
            "and b.room.id = :roomId " +
            "and :dateFrom <= b.dateTo and b.dateFrom <= :dateTo")
    List<BookingEntity> getOverlappingBookings(UUID roomId, LocalDate dateFrom, LocalDate dateTo);
}
