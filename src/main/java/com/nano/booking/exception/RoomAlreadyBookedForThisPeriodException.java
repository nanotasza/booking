package com.nano.booking.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public class RoomAlreadyBookedForThisPeriodException extends RuntimeException {
}
