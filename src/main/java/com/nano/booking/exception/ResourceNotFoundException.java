package com.nano.booking.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public class ResourceNotFoundException extends RuntimeException {
    private String entityName;

    public ResourceNotFoundException(String entityName) {
        super(entityName + " not found");
        this.entityName = entityName;
    }
}
