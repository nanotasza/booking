drop table if exists booking;
drop table if exists customer;
drop table if exists room;
drop table if exists hotel;

create table customer
(
    id uuid not null
        constraint customer_pkey
            primary key,
    first_name varchar(255),
    last_name varchar(255)
);

create table hotel
(
    id uuid not null
        constraint hotel_pkey
            primary key,
    city varchar(255)
);

create table room
(
    id uuid not null
        constraint room_pkey
            primary key,
    price_per_night numeric(19,2),
    hotel_id uuid
        constraint fkdosq3ww4h9m2osim6o0lugng8
            references hotel
);

create table booking
(
    id uuid not null
        constraint booking_pkey
            primary key,
    booking_status integer not null,
    date_from date not null,
    date_to date not null,
    customer_id uuid not null
        constraint fklnnelfsha11xmo2ndjq66fvro
            references customer,
    room_id uuid not null
        constraint fkq83pan5xy2a6rn0qsl9bckqai
            references room
);
