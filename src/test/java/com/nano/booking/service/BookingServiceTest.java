package com.nano.booking.service;

import com.nano.booking.entity.BookingEntity;
import com.nano.booking.entity.CustomerEntity;
import com.nano.booking.entity.RoomEntity;
import com.nano.booking.exception.ResourceNotFoundException;
import com.nano.booking.exception.RoomAlreadyBookedForThisPeriodException;
import com.nano.booking.model.BookingDto;
import com.nano.booking.model.BookingStatus;
import com.nano.booking.repository.BookingRepository;
import com.nano.booking.repository.CustomerRepository;
import com.nano.booking.repository.RoomRepository;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collections;
import java.util.Optional;

import static com.nano.booking.fixture.TestFixture.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest({BookingService.class})
public class BookingServiceTest {

    private BookingRepository bookingRepository = Mockito.mock(BookingRepository.class);
    private CustomerRepository customerRepository = Mockito.mock(CustomerRepository.class);
    private RoomRepository roomRepository = Mockito.mock(RoomRepository.class);
    private BookingService bookingService = new BookingService(bookingRepository, customerRepository, roomRepository);

    @Rule
    ExpectedException expectedException = ExpectedException.none();

    @Test
    public void bookingIsCreatedWithNoExceptions(){
        BookingDto bookingDto = new BookingDto();
        bookingDto.setDateFrom(DATE_FROM);
        bookingDto.setDateTo(DATE_TO);
        bookingDto.setCustomerId(CUSTOMER_ID);
        bookingDto.setRoomId(ROOM_ID);

        Mockito.when(bookingRepository.getOverlappingBookings(ROOM_ID, DATE_FROM, DATE_TO)).thenReturn(Collections.emptyList());
        Mockito.when(customerRepository.findById(CUSTOMER_ID)).thenReturn(Optional.of(new CustomerEntity()));
        Mockito.when(roomRepository.findById(ROOM_ID)).thenReturn(Optional.of(new RoomEntity()));

        bookingService.createBooking(bookingDto);
    }

    @Test
    public void exceptionIsThrownIfPeriodIsOverlapping(){
        BookingDto bookingDto = new BookingDto();
        bookingDto.setDateFrom(DATE_FROM);
        bookingDto.setDateTo(DATE_TO);
        bookingDto.setCustomerId(CUSTOMER_ID);
        bookingDto.setRoomId(ROOM_ID);

        Mockito.when(bookingRepository.getOverlappingBookings(ROOM_ID, DATE_FROM, DATE_TO)).thenReturn(Collections.singletonList(new BookingEntity()));

        expectedException.expect(RoomAlreadyBookedForThisPeriodException.class);

        bookingService.createBooking(bookingDto);
    }

    @Test
    public void exceptionIsThrownIfCustomerDoesNotExist(){
        BookingDto bookingDto = new BookingDto();
        bookingDto.setDateFrom(DATE_FROM);
        bookingDto.setDateTo(DATE_TO);
        bookingDto.setCustomerId(CUSTOMER_ID);
        bookingDto.setRoomId(ROOM_ID);

        Mockito.when(bookingRepository.getOverlappingBookings(ROOM_ID, DATE_FROM, DATE_TO)).thenReturn(Collections.emptyList());
        Mockito.when(customerRepository.findById(CUSTOMER_ID)).thenReturn(Optional.empty());

        expectedException.expect(ResourceNotFoundException.class);
        expectedException.expectMessage("customer not found");

        bookingService.createBooking(bookingDto);
    }

    @Test
    public void exceptionIsThrownIfRoomDoesNotExist(){
        BookingDto bookingDto = new BookingDto();
        bookingDto.setDateFrom(DATE_FROM);
        bookingDto.setDateTo(DATE_TO);
        bookingDto.setCustomerId(CUSTOMER_ID);
        bookingDto.setRoomId(ROOM_ID);

        Mockito.when(bookingRepository.getOverlappingBookings(ROOM_ID, DATE_FROM, DATE_TO)).thenReturn(Collections.emptyList());
        Mockito.when(customerRepository.findById(CUSTOMER_ID)).thenReturn(Optional.of(new CustomerEntity()));
        Mockito.when(roomRepository.findById(ROOM_ID)).thenReturn(Optional.empty());

        expectedException.expect(ResourceNotFoundException.class);
        expectedException.expectMessage("room not found");

        bookingService.createBooking(bookingDto);
    }

    @Test
    public void bookingIsUpdatedWithNoExceptions(){
        BookingDto bookingDto = new BookingDto();
        bookingDto.setDateFrom(DATE_FROM);
        bookingDto.setDateTo(DATE_TO);
        bookingDto.setCustomerId(CUSTOMER_ID);
        bookingDto.setRoomId(ROOM_ID);
        bookingDto.setBookingStatus(BookingStatus.CANCELLED);

        BookingEntity bookingEntity = new BookingEntity();

        Mockito.when(bookingRepository.findById(BOOKING_ID)).thenReturn(Optional.of(bookingEntity));

        bookingService.updateBooking(BOOKING_ID, bookingDto);

        Assertions.assertThat(bookingEntity.getBookingStatus()).isEqualTo(BookingStatus.CANCELLED);
    }

    @Test
    public void exceptionIsThrownWhenBookingDoesNotExist(){
        BookingDto bookingDto = new BookingDto();
        bookingDto.setDateFrom(DATE_FROM);
        bookingDto.setDateTo(DATE_TO);
        bookingDto.setCustomerId(CUSTOMER_ID);
        bookingDto.setRoomId(ROOM_ID);
        bookingDto.setBookingStatus(BookingStatus.CANCELLED);

        BookingEntity bookingEntity = new BookingEntity();

        Mockito.when(bookingRepository.findById(BOOKING_ID)).thenReturn(Optional.empty());
        expectedException.expect(ResourceNotFoundException.class);
        expectedException.expectMessage("booking not found");

        bookingService.updateBooking(BOOKING_ID, bookingDto);
    }
}

