package com.nano.booking.repository;

import com.nano.booking.entity.CustomerEntity;
import com.nano.booking.entity.HotelEntity;
import com.nano.booking.entity.RoomEntity;
import com.nano.booking.model.BookingStatus;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Set;

import static com.nano.booking.fixture.TestFixture.*;


@ExtendWith(SpringExtension.class)
@DataJpaTest
class BookingRepositoryTest {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private RoomRepository roomRepository;

    @Test
    void allRoomsAreReturnedIfNoneIsBooked(){
        // given
        insertHotelAndRooms();

        // when
        Set<RoomEntity> matchingRooms = roomRepository.getMatchingRooms(CITY_WARSAW, DATE_FROM, DATE_TO, PRICE_FROM, PRICE_TO);

        // then
        Assertions.assertThat(matchingRooms.size()).isEqualTo(2);
    }

    @Test
    void noRoomsAreReturnedIfPriceIsOutOfRange(){
        // given
        insertHotelAndRooms();

        // when
        Set<RoomEntity> matchingRooms = roomRepository.getMatchingRooms(CITY_WARSAW, DATE_FROM, DATE_TO, BigDecimal.ZERO, BigDecimal.TEN);

        // then
        Assertions.assertThat(matchingRooms).isEmpty();
    }

    @Test
    void noRoomsAreReturnedIfNoneInExpectedCity(){
        // given
        insertHotelAndRooms();

        // when
        Set<RoomEntity> matchingRooms = roomRepository.getMatchingRooms("Jakarta", DATE_FROM, DATE_TO, PRICE_FROM, PRICE_TO);

        // then
        Assertions.assertThat(matchingRooms).isEmpty();
    }

    @Test
    void oneRoomIsReturnedIfTheOtherRoomIsBookedInThisPeriod(){
        // given
        insertBooking(BookingStatus.CONFIRMED);

        // when
        Set<RoomEntity> matchingRooms = roomRepository.getMatchingRooms(CITY_WARSAW, DATE_FROM, DATE_TO, PRICE_FROM, PRICE_TO);

        // then
        Assertions.assertThat(matchingRooms.size()).isEqualTo(1);
    }

    @Test
    void twoRoomsAreReturnedIfBookingForTheSecondRoomIsCancelled(){
        // given
        insertBooking(BookingStatus.CANCELLED);

        // when
        Set<RoomEntity> matchingRooms = roomRepository.getMatchingRooms(CITY_WARSAW, DATE_FROM, DATE_TO, PRICE_FROM, PRICE_TO);

        // then
        Assertions.assertThat(matchingRooms.size()).isEqualTo(2);
    }

    @Test
    void twoRoomsAreReturnedIfDatesDontOverlapWithExistingBooking(){
        // given
        insertBooking(BookingStatus.CONFIRMED);

        // when
        Set<RoomEntity> matchingRooms = roomRepository.getMatchingRooms(CITY_WARSAW, EARLIER_DATE_FROM, EARLIER_DATE_TO, PRICE_FROM, PRICE_TO);

        // then
        Assertions.assertThat(matchingRooms.size()).isEqualTo(2);
    }

    @Test
    void oneRoomIsReturnedIfDateFromIsOverlapping(){
        // given
        insertBooking(BookingStatus.CONFIRMED);

        // when
        Set<RoomEntity> matchingRooms = roomRepository.getMatchingRooms(CITY_WARSAW, EARLIER_DATE_FROM, DATE_MIDDLE, PRICE_FROM, PRICE_TO);

        // then
        Assertions.assertThat(matchingRooms.size()).isEqualTo(1);
    }

    @Test
    void oneRoomIsReturnedIfDateToIsOverlapping(){
        // given
        insertBooking(BookingStatus.CONFIRMED);

        // when
        Set<RoomEntity> matchingRooms = roomRepository.getMatchingRooms(CITY_WARSAW, DATE_MIDDLE, LATER_DATE_TO, PRICE_FROM, PRICE_TO);

        // then
        Assertions.assertThat(matchingRooms.size()).isEqualTo(1);
    }

    private void insertBooking(BookingStatus bookingStatus) {
        CustomerEntity customerEntity = getCustomerEntity();
        HotelEntity hotelEntity = getHotelEntity();
        RoomEntity roomEntity = getRoomEntity(100.00, hotelEntity);
        entityManager.persist(hotelEntity);
        entityManager.persist(customerEntity);
        entityManager.persist(roomEntity);
        entityManager.persist(getRoomEntity(150.00, hotelEntity));
        entityManager.persist(getBookingEntity(roomEntity, customerEntity, bookingStatus));
    }

    private void insertHotelAndRooms() {
        HotelEntity hotelEntity = getHotelEntity();
        entityManager.persist(hotelEntity);
        entityManager.persist(getRoomEntity(100.00, hotelEntity));
        entityManager.persist(getRoomEntity(150.00, hotelEntity));

        entityManager.flush();
    }

    private RoomEntity getRoomEntity(double v, HotelEntity hotelEntity) {
        RoomEntity roomEntity = new RoomEntity();
        roomEntity.setHotel(hotelEntity);
        roomEntity.setPricePerNight(BigDecimal.valueOf(v));
        return roomEntity;
    }

}
