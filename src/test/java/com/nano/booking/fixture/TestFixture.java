package com.nano.booking.fixture;

import com.nano.booking.entity.BookingEntity;
import com.nano.booking.entity.CustomerEntity;
import com.nano.booking.entity.HotelEntity;
import com.nano.booking.entity.RoomEntity;
import com.nano.booking.model.BookingStatus;

import java.awt.print.Book;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class TestFixture {
    public static final UUID ROOM_ID = UUID.fromString("ed36d4c2-281a-4f82-a378-ca2c27faf9fc");
    public static final UUID CUSTOMER_ID = UUID.fromString("dd6cf638-0eb5-4106-9fef-2aac14e8993d");
    public static final UUID BOOKING_ID = UUID.fromString("ee6cf638-0eb5-4106-9fef-2aac14e8993d");
    public static final LocalDate DATE_FROM = LocalDate.parse("2019-11-01", DateTimeFormatter.ISO_DATE);
    public static final LocalDate DATE_MIDDLE= LocalDate.parse("2019-11-05", DateTimeFormatter.ISO_DATE);
    public static final LocalDate DATE_TO = LocalDate.parse("2019-11-10", DateTimeFormatter.ISO_DATE);
    public static final LocalDate EARLIER_DATE_FROM = LocalDate.parse("2019-10-01", DateTimeFormatter.ISO_DATE);
    public static final LocalDate EARLIER_DATE_TO = LocalDate.parse("2019-10-10", DateTimeFormatter.ISO_DATE);
    public static final LocalDate LATER_DATE_TO = LocalDate.parse("2019-11-10", DateTimeFormatter.ISO_DATE);
    public static final String CITY_WARSAW = "Warsaw";
    public static final BigDecimal PRICE_FROM = BigDecimal.valueOf(50.00);
    public static final BigDecimal PRICE_TO = BigDecimal.valueOf(200.00);


    public static HotelEntity getHotelEntity() {
        HotelEntity hotelEntity = new HotelEntity();
        hotelEntity.setCity(CITY_WARSAW);
        return hotelEntity;
    }

    public static CustomerEntity getCustomerEntity(){
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setFirstName("Emily");
        customerEntity.setLastName("Elderberry");
        return customerEntity;
    }

    public static BookingEntity getBookingEntity(RoomEntity roomEntity, CustomerEntity customerEntity, BookingStatus bookingStatus) {
        BookingEntity bookingEntity = new BookingEntity();
        bookingEntity.setRoom(roomEntity);
        bookingEntity.setDateFrom(DATE_FROM);
        bookingEntity.setDateTo(DATE_TO);
        bookingEntity.setCustomer(customerEntity);
        bookingEntity.setBookingStatus(bookingStatus);
        return bookingEntity;
    }
}
